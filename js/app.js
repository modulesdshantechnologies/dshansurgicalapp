var webapp = angular.module('webapp', ['ui.router']);

webapp.config(function($stateProvider,  $urlRouterProvider) {

    $urlRouterProvider.otherwise('/home');

    $stateProvider

    // HOME STATES AND NESTED VIEWS ========================================
        .state('home', {
            url: '/home',
            templateUrl: 'home.html',
            controller:'HeaderController'
        })

        // ABOUT PAGE AND MULTIPLE NAMED VIEWS =================================
        .state('about', {
            url: '/about',
            templateUrl: 'about.html'

            // we'll get to this in a bit
        })

        .state('hotel', {
            url: '/services',
            templateUrl: 'hotel.html'
        })


        .state('shortcodes', {
            url: '/shortcodes',
            templateUrl: 'shortcodes.html'
        })


        .state('gallery', {
            url: '/gallery',
            templateUrl: 'gallery.html'
        })


        .state('contact', {
            url: '/contact',
            templateUrl: 'contact.html'
        });


});